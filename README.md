# Firestore Angular Starter

by CoderbaseIT 

[![pipeline status](https://gitlab.com/formation-pro1/formation-angular/18-19-juin-2020/angular-advanced-support/badges/master/pipeline.svg)](https://gitlab.com/formation-pro1/formation-angular/18-19-juin-2020/angular-advanced-support/-/commits/master)
[![Documentation Status](https://readthedocs.org/projects/ansicolortags/badge/?version=latest)](https://compodoc.web.app/)

![npm](https://badgen.net/badge/icon/6.14.4?icon=npm&label&color=red)  ![typescript](https://badgen.net/badge/icon/3.8.3?icon=typescript&label&color=blue)

![Maintenance](https://img.shields.io/badge/Maintained%3F-yes-green.svg)

![ForTheBadge built-by-developers](http://ForTheBadge.com/images/badges/built-by-developers.svg)
![ForTheBadge built-with-love](http://ForTheBadge.com/images/badges/built-with-love.svg)

[TOC]

## DEMOS 
#### Catalogue:  TODO APP CATALOGUE

- [App](https://bundle-analyze.web.app)
- [Doc](https://compodoc.web.app)
- [Bundle Analyze](https://bundle-analyze.web.app)
- [Pay11](https://pa11y-ci.web.app/)

## PREREQUIS : 

| Starter | Angular |
| :-----: | :-----: |
|  0.0.2  |    9    |


## How to use

> This is a starter project for Angular App.
> It is a full fledged Angular workspace with demo application.
> It is designed to be used for strong starter with Angualr
> you'd need ready for CI, code coverage, SSR testing, StackBlitz demo deployment and more.

1. Run `npm install` or `yarn install` to install everything.
2. Create a new project on [firebase console](https://console.firebase.google.com/) and attach web firebase application to it.
3. Copy firebase configuration to angular environment files. 
4. Update Authentication providers part for your firebase project , Activate Google and email/password provider.
5. Create Firestore Database with rules on mode test.
6. Run `npm install -g firebase-tools` 
7.  Run `firebase login:ci` , Follow ouput link for attach to google account used with firebase throught browser 
8.  Copy the token from terminal  in your gitlab settings/variables with key FIREBASE_DEPLOY_KEY and select option masked
9. Search inside all files of starter project with your IDE and rename **firestore-angular-starter** to the name of your unique firebase project name

## :rocket: Cool features :rocket:

### :books: ​[Compodoc](https://compodoc.app/) :books:

[https://compodoc.app/](https://compodoc.app/) is great tools for auto-generate documentation with markdown integration , overview ,  code source , documentation report, visual Angular urls and more ...

Compodoc CI/CD is part of the starter.

1. Inside your firebase project , go to hosting and create a different site than principal one .
2. Use the name of freshly create site and replace `compodoc`
3. TODO changer le nom compodoc utilisé par firestore-angular-starter-compodoc

### :eyes: pa11y Accessibility :eyes:

Accessibility is manage by pa11y-ci

1. Create firebase sub-site and replace "pa11y-ci" with your firebase site name
2. Ci is already prepare with gitlab
3. Open .pa11yci and add urls that you want to analyze.
4. Report will be display on ur firebase site name hosting 



### :mag: Bundle Analyze :mag:

Bundle Analyze use webpack-bundle-analyzer

### Local :

`npm run bundleAnalyzer`

### CI/CD : 

1. Create firebase sub-site and replace "bundle-analyze" with your firebase site name
2. Ci is already prepare with gitlab
3. Commit
4. Report will be display on ur firebase site name hosting 

   
### :floppy_disk: Versionning :floppy_disk:

## TODO: Partie versionning  

1.  Versioning is ready for you with following simple commands:

    ```json
        "release": "standard-version",
        "release:patch": "npm run release -- --release-as patch",
        "release:minor": "npm run release -- --release-as minor",
        "release:major": "npm run release -- --release-as major",
        "publish": "npm run build:all && npm publish:all"
    ```

    Just use [Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0-beta.4/)
    format and `CHANGELOG.md` will be automatically generated on releases by
    [Standard Version](https://github.com/conventional-changelog/standard-version#standard-version).

2.  This project has Angular Universal —
    you can easily test your library with SSR and Prerender:

    `npm run start:ssr` or `npm run start:prerender`

3.  CI and code coverage are ready, configured to use
    [Travis](http://travis-ci.org/) and [Coveralls](https://coveralls.io).
    Just authorize on [Travis](http://travis-ci.org/) with your Github account
    and enable your repository and do the same on [Coveralls](https://coveralls.io).
    If you want to have more than one library in your workspace add
    `COVERALLS_PARALLEL=true` to [Travis](http://travis-ci.org/) Environmental variables.

4.  Precommit checks, prettier, linter and all that jazz is there.

5.  You can also deploy your demo to [StackBlitz](https://stackblitz.com) with no hustle,
    just use link in the following format:

        https://stackblitz.com/github/[User|Organization]/[Repository]/tree/master/projects/demo

6.  You can add more libraries using the same `npm run add` command to create a whole Angular Workspace
    with multiple libraries. Versioning and publishing is configured that they are released simultaneously like Angular packages.



## QuickStart

## TODO: Partie TU 

### Ajout de Jest au projet
### Supression de Jasmine/Karma

## TODO: Partie E2E
## TODO: Partie pwa
## TODO: Partie ssr
## TODO: Partie csr
## TODO: Partie scully
## TODO: Partie pwa
## TODO: Partie serverless (déployement cloud agnotics)
## TODO: partie lintage
## TODO: partie Sonar
## TODO: couverture code 
## TODO: couverture de test 

## Lighthouse 	

- [Configuration](https://github.com/GoogleChrome/lighthouse/blob/master/docs/configuration.md)
- [CLI](https://github.com/andreasonny83/lighthouse-ci#cli)

## Sécurité 
**Pensez à vos règles de base de données c'est ici votre seul réel protection.**

**Cette partie est géré dans le fichier firestore.rules et database.rules**

## Les commandes pour le dev 

### Partie Intégration Third App
**coté back, le code pour communiquer avec slack est présent et permet l'envoi de message depuis le back end vers votre channel slack 
il vous faut créer un webhook slack (url permettant l'accès à votre channel) pour cette partie
voir functions/use/slack/notif**


### Multi environnement 
**Le projet intégré facilement l'ajout d'un environnement de dev distant ou tout autre type d'environnement tel que dev, UAT , IPU , QA , PREPROD , PROD**  


## TODO

- [ ] liens vers les sous sites et de la démo
- [ ] Intégration continue
- [ ] Badges 
- [ ] infos et features 
