import {Directive, HostBinding, HostListener, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';

@Directive({
  selector: '[appStateColor]'
})
export class StateColorDirective implements OnInit, OnChanges {
  @Input() state;

  @HostBinding('class') myClass;
  constructor() { }

  // @HostListener('click')
  //   // performAction() {
  //   //   alert('Vous avez cliqué');
  //   // }


  ngOnInit(): void {

  }

  ngOnChanges(changes: SimpleChanges): void {
    this.myClass = `app-${this.state.toLowerCase()}`;
  }

}
