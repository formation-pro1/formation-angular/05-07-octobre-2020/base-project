import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../shared/services/auth.service';
import { FormBuilder, Validators } from '@angular/forms';
import * as dayjs from 'dayjs';
@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css']
})

export class SignInComponent implements OnInit {
  dateOfToday = dayjs();
  myForm: any;

  constructor(
    public authService: AuthService,
    private fb: FormBuilder
  ) { }

  ngOnInit() {
    this.createForm();
   }

   createForm() {
     this.myForm = this.fb.group({
       email: ['', Validators.compose([Validators.email , Validators.required])],
       password: ['',  Validators.required],
     });
   }

  async SignIn() {
    const { email : userName, password: userPassword} = this.myForm.value;
    await this.authService.SignIn(userName, userPassword).catch((error) => console.log(error));
  }

}
