import {HttpClient, HttpClientModule} from '@angular/common/http';
import {LOCALE_ID, NgModule} from '@angular/core';
// Firebase services + environment module
import {AngularFireModule} from '@angular/fire';
import {AngularFireAuthModule} from '@angular/fire/auth';
import {AngularFireAuthGuardModule} from '@angular/fire/auth-guard';
import {AngularFirestoreModule} from '@angular/fire/firestore';
import {AngularFirePerformanceModule} from '@angular/fire/performance';
// Reactive Form
import {ReactiveFormsModule} from '@angular/forms';
import {BrowserModule} from '@angular/platform-browser';
import {NotifierModule} from 'angular-notifier';
import {NgxUiLoaderHttpModule, NgxUiLoaderModule} from 'ngx-ui-loader';
import {environment} from '../environments/environment';
// App routing modules
import {AppRoutingModule} from './app-routing.module';
// App components
import {AppComponent} from './app.component';
import {AuthModule} from './auth/auth.module';
import {ngxUiLoaderConfig} from './shared/configs/ngxloader.config';
import {notifierDefaultOptions} from './shared/configs/notifier.config';
import {SharedModule} from './shared/shared.module';
import {registerLocaleData} from '@angular/common';
import {NgbDateAdapter, NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {NgbDateFirestoreAdapter} from './shared/services/ngb-date-firestore-adapter.service';
import {ServiceWorkerModule} from '@angular/service-worker';

import localeFr from '@angular/common/locales/fr';

// the second parameter 'fr' is optional
registerLocaleData(localeFr, 'fr');

@NgModule({
  declarations: [
    AppComponent

  ],
  imports: [
    BrowserModule,
    AuthModule,
    AppRoutingModule,
    SharedModule,
    AngularFirePerformanceModule,
    NotifierModule.withConfig(notifierDefaultOptions()),
    NgxUiLoaderModule.forRoot(ngxUiLoaderConfig()),
    NgxUiLoaderHttpModule,
    HttpClientModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireAuthModule,
    AngularFirestoreModule,
    AngularFireAuthGuardModule,
    ReactiveFormsModule,
    NgbModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
  ],
  bootstrap: [AppComponent],
  providers: [
    {provide: LOCALE_ID , useValue: 'fr'},
    {provide: NgbDateAdapter, useClass: NgbDateFirestoreAdapter}
  ]
})

export class AppModule { }
